
import json
# Opening JSON file
f = open('D:\Downloads\PetOwners2.json',)
# returns JSON object as 
# a dictionary
data = json.load(f)
petOwners={}

# Iterating through the json
# list
pets={}
for i in data:
    petOwners[f'{i["id"]}']={'name': i['name'], 'email': i['email'], 'gender': i['gender'], 'stars': i['stars'], 'minCalificacionReqq': i['minCalificacionReqq'], 'maxNumberPetsAccepted': i['maxNumberPetsAccepted']}
    for pet in i['pets']:
        # print(pet['name'])
        pets[pet['name']]=pet
    # print(pets)
    petOwners[f'{i["id"]}']["__collections__"]= {"pets":{}}
    petOwners[f'{i["id"]}']["__collections__"]["pets"]= pets
    pets={}
  
# Closing file
print(json.dumps(petOwners))
f.close()

# {'id': 990, 'name': 'Clerissa Olenchenko', 'email': 'colenchenkori@disqus.com', 'gender': 'Polygender', 'stars': 1, 'minCalificacionReqq': 1, 'maxNumberPetsAccepted': 0, 
# 'pets': [{'name': 'Alano', 'type': 'cat', 'size': 7, 'minSpaceRequired': 700, 'acceptDogs': True, 'acceptCats': True}, 
# {'name': 'Forest', 'type': 'dog', 'size': 10, 'minSpaceRequired': 1000, 'acceptDogs': False, 'acceptCats': False}]}