
import json
# Opening JSON file
f = open('D:\Downloads\PetCaretakers.json',)
# returns JSON object as 
# a dictionary
data = json.load(f)
careTakers={}

# Iterating through the json
# list
pets={}
for i in data:
    careTakers[i["id"]]={
    'name': i['name'], 
    'email': i['email'], 
    "address": i["address"],
    'gender': i['gender'], 
    'stars': i['stars'], 
    "avaliableSpaceMtSq": i["avaliableSpaceMtSq"], 
    }
    for pet in i['pets']:
        # print(pet['name'])
        pets[pet['name']]=pet
    # print(pets)
    careTakers[i["id"]]["__collections__"]= {"pets":{}}
    careTakers[i["id"]]["__collections__"]["pets"]= pets
    pets={}
  
# Closing file
print(json.dumps(careTakers))
f.close()

# #     {
#         "id": 0,
#         "name": "Walden Mainstone",
#         "email": "wmainstone0@vistaprint.com",
#         "address": "08 Talmadge Junction",
#         "gender": "Genderqueer",
#         "stars": 2,
#         "avaliableSpaceMtSq": 85,
#         "pets": []
#     },
#     {
#         "id": 1,
#         "name": "Richmond Dullingham",
#         "email": "rdullingham1@who.int",
#         "address": "5634 Anderson Plaza",
#         "gender": "Agender",
#         "stars": 2,
#         "avaliableSpaceMtSq": 158,
#         "pets": [
#             {
#                 "name": "Carny",
#                 "type": "cat",
#                 "size": 9
#             }
#         ]
#     }