import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AppUser with ChangeNotifier {
  final User user;

  AppUser({this.user});

  set user(User newUser) {
    this.user = newUser;
    notifyListeners();
  }
}
