import 'package:flutter/material.dart';

class NavBarIndexes with ChangeNotifier {
  int lastIndex;
  int myIndex;

  NavBarIndexes({this.lastIndex, this.myIndex});

  int get getLastIndex => this.lastIndex;

  set setLastIndex(int lastIndex) {
    this.lastIndex = lastIndex;
    notifyListeners();
  }

  int get getMyIndex => this.myIndex;

  set setMyIndex(int myIndex) {
    this.myIndex = myIndex;
    notifyListeners();
  }
}
