import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'Home/home.dart';
import 'welcome/welcome_screen.dart';

class Wrapper extends StatefulWidget {
  const Wrapper({Key key}) : super(key: key);

  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  // Widget _screen;
  // String _screenname;
  // User userFromFB;

  @override
  void initState() {
    super.initState();
    // FirebaseCrashlytics.instance.crash();
    // FirebaseCrashlytics.instance.log('This is a log example');
    // FirebaseCrashlytics.instance.recordError(e, s, reason: 'as an example')

    FirebaseAuth.instance.authStateChanges().listen((firebaseUser) {
      if (firebaseUser == null) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) {
                return WelcomeScreen();
              },
              settings: RouteSettings(name: "WelcomeScreen")),
        );
        // setState(() {
        //   print("go to welcome screen from init state method");
        //   _screen = WelcomeScreen();
        //   _screenname = "Welcome";
        //   userFromFB = firebaseUser;
        // });
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) {
                return Home();
              },
              settings: RouteSettings(name: "Home")),
        );
        // setState(() {
        //   print("firebaseUser go to home ${firebaseUser}");
        //   _screen = Home();
        //   _screenname = "Home";
        //   userFromFB = firebaseUser;
        // });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
