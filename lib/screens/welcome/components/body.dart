import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
import 'package:petbnbpn/screens/login/login_screen.dart';
import 'package:petbnbpn/screens/signup/signup_screen.dart';
import 'package:provider/provider.dart';
import '../../../components/rounded_button.dart';
import '../../../constants/app_constants.dart';
import '../components/background.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "WELCOME TO PETBNB",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.05),
            SvgPicture.asset(
              "assets/icons/chat.svg",
              height: size.height * 0.45,
            ),
            SizedBox(height: size.height * 0.05),
            RoundedButton(
              text: "LOGIN",
              color: kPrimaryColor,
              press: () {
                FirebaseAnalytics()
                    .logEvent(name: "pressedLoginButtonAtWelcomeScreen");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                      settings: RouteSettings(name: "LoginScreen")),
                );
              },
            ),
            RoundedButton(
              text: "SIGN UP",
              color: kPrimaryLightColor,
              textColor: currentTheme.primaryColor,
              press: () {
                FirebaseAnalytics()
                    .logEvent(name: "pressedRegisterButtonAtWelcomeScreen");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) {
                        return SignUpScreen();
                      },
                      settings: RouteSettings(name: "SignUpScreen")),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
