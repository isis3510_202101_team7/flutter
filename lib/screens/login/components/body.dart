import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:petbnbpn/components/already_have_an_account_acheck.dart';
import 'package:petbnbpn/components/rounded_button.dart';
import 'package:petbnbpn/components/rounded_input_field.dart';
import 'package:petbnbpn/components/rounded_password_field.dart';
import 'package:petbnbpn/models/user.dart';
import 'package:petbnbpn/screens/login/components/background.dart';
import 'package:petbnbpn/screens/signup/signup_screen.dart';
import 'package:petbnbpn/services/auth.dart';
import 'package:provider/provider.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String _email;
  String _password;
  String _error;

  @override
  void initState() {
    super.initState();
    this._email = "";
    this._password = "";
    this._error = "";
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final AuthService _auth = AuthService();
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              "assets/icons/login.svg",
              height: size.height * 0.35,
            ),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {
                setState(() {
                  this._email = value;
                });
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                this._password = value;
              },
            ),
            RoundedButton(
              text: "LOGIN",
              press: () async {
                FirebaseAnalytics()
                    .logEvent(name: "pressedLoginButtonAtLoginScreen");
                //
                try {
                  dynamic result = await _auth.logInEmailAndPassword(
                      email: this._email.trim(), pass: this._password);
                  if (result.runtimeType == String) {
                    setState(() {
                      this._error = result;
                    });
                    print(result);
                  } else {
                    setState(() {
                      this._error = "Error while Loging in ${result}";
                    });
                    print("error signing in");
                  }
                } catch (e) {
                  setState(() {
                    this._error = "Error while Loging in";
                  });
                  print(e);
                }

                // dynamic result = await _auth.signInAnon();
                // if (result == null) {
                //   print("error signing in");
                // } else {
                //   print("signed in");
                //   // Provider.of<AppUser>(context, listen: false).user =
                // }
              },
            ),
            Center(
                child: Text(
              this._error != null ? this._error : "",
              style: TextStyle(color: Colors.redAccent),
            )),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) {
                        return SignUpScreen();
                      },
                      settings: RouteSettings(name: "SignUpScreen")),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
