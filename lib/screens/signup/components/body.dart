import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:petbnbpn/services/auth.dart';
import '../../../components/already_have_an_account_acheck.dart';
import '../../../components/rounded_button.dart';
import '../../../components/rounded_input_field.dart';
import '../../../components/rounded_password_field.dart';
import '../../login/login_screen.dart';
import './background.dart';
import './or_divider.dart';
import 'social_icon.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String _email;
  String _password;
  String _error;

  @override
  void initState() {
    super.initState();
    this._email = "";
    this._password = "";
    this._error = "";
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final AuthService _auth = AuthService();
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              "assets/icons/signup.svg",
              height: size.height * 0.35,
            ),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {
                setState(() {
                  this._email = value;
                });
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                this._password = value;
              },
            ),
            RoundedButton(
              text: "SIGNUP",
              press: () async {
                // print(this._email);
                // print(this._password);
                try {
                  // FirebaseCrashlytics.instance.crash();
                  dynamic result = await _auth.registration(
                      email: this._email.trim(), pass: this._password);
                  if (result.runtimeType == String) {
                    setState(() {
                      this._error = result;
                    });
                    print(result);
                  } else {
                    setState(() {
                      this._error = "Error while Signing Up";
                    });
                    print("error signing up");
                  }
                } catch (e) {
                  print(e);
                }
              },
            ),
            Center(
                child: Text(
              this._error != null ? this._error : "",
              style: TextStyle(color: Colors.redAccent),
            )),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                FirebaseAnalytics()
                    .logEvent(name: "pressedRegisterButtonAtSignUpScreen");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                      settings: RouteSettings(name: "LoginScreen")),
                );
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/icons/facebook.svg",
                  press: () {
                    FirebaseAnalytics()
                        .logEvent(name: "TriedToLoginOnTwitterFb");
                  },
                ),
                SocalIcon(
                  iconSrc: "assets/icons/twitter.svg",
                  press: () {
                    FirebaseAnalytics().logEvent(name: "TriedToLoginOnTwitter");
                  },
                ),
                SocalIcon(
                  iconSrc: "assets/icons/google-plus.svg",
                  press: () {
                    FirebaseAnalytics().logEvent(name: "TriedToLoginOnGoogle");
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
