import 'package:flutter/material.dart';
import 'package:petbnbpn/components/card1.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
import 'package:petbnbpn/services/firestorage.dart';
import 'package:petbnbpn/services/realDb.dart';
import 'package:petbnbpn/services/recommendations.dart';
import 'package:provider/provider.dart';

class Recommendation extends StatefulWidget {
  const Recommendation({Key key}) : super(key: key);

  @override
  _RecommendationState createState() => _RecommendationState();
}

class _RecommendationState extends State<Recommendation> {
  final DBService dBService = DBService();
  final FirestorageService firestoreService = FirestorageService();
  final RecommendationsService recommendationService = RecommendationsService();

  var filteredCareTakers = [];
  var petOwner;
  final int minCalificacionReq = 4;
  final int minSpaceReq =
      400; //since it is the biggest dog a person could have in a scale from 1 to 10
  final bool acceptDogs = false;
  final bool acceptCats = false;
  final int maxNumPetsAccepted = 0;
  var subscription;

  @override
  void initState() {
    super.initState();
    // firestoreService
    //     .getCollection(path: "petOwners")
    //     .then((value) => print(value.docs.map((doc) => doc.data()).toList()));

    // Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
    //   setState(() {
    //     print(subscription);
    //     subscription = result;
    //   });
    // });

//     var connectivityResult = await (Connectivity().checkConnectivity());
// if (connectivityResult == ConnectivityResult.mobile) {
//   // I am connected to a mobile network.
// } else if (connectivityResult == ConnectivityResult.wifi) {
//   // I am connected to a wifi network.
// }

    dBService
        .getFromFirebaseRTDB(path: "/petOwnersData/4")
        .then((petOwnerP) => {
              recommendationService
                  .filteredCareTakers(petOwner: petOwnerP)
                  .then((value) => setState(() {
                        filteredCareTakers = value;
                        petOwner = petOwnerP;
                      }))
            });
    // dBService.getFromFirebaseRTDB(path: "/petOwners/4");
  }

  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: size.height * 0.17,
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Caretaker compatibility',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: currentTheme.primaryColor),
                ),
              ),
              Expanded(
                child: petOwner != null
                    ? SingleChildScrollView(
                        child: RichText(
                          text: TextSpan(
                            style: TextStyle(
                                fontSize: 12.0,
                                color: currentTheme.primaryColor),
                            children: <TextSpan>[
                              TextSpan(
                                  text:
                                      'My pet is a member of my family, so the candidates must have a rating of '),
                              TextSpan(
                                  text: petOwner['minCalificacionReqq']
                                      .toString(),
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text:
                                      ' stars of higher. The aspirant should own a maximum of '),
                              TextSpan(
                                  text: petOwner['maxNumberPetsAccepted']
                                      .toString(),
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text:
                                      ' pets in the house, and a minimum of  '),
                              TextSpan(
                                  text: petOwner['pets'][0]['minSpaceRequired']
                                      .toString(),
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text:
                                      ' sqm so my dear ${petOwner['pets'][0]['name']} has plenty of room to goof around. Furthermore, I am sure '),
                              TextSpan(
                                  text:
                                      '${petOwner['pets'][0]['name']} would '),
                              TextSpan(
                                  text:
                                      "${petOwner['pets'][0]['acceptDogs'] ? "like" : "not like"}",
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text:
                                      ' to be around dogs, and ${petOwner['pets'][0]['name']} '),
                              TextSpan(
                                  text:
                                      "${petOwner['pets'][0]['acceptCats'] ? "does not mind cats" : "would not like cats neither"}'",
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              // TextSpan(text: ' to be around dogs, and '),
                            ],
                          ),
                        ),
                      )
                    : Center(child: CircularProgressIndicator()),
              )
            ],
          ),
        ),
        Container(
          child: Expanded(
            child: filteredCareTakers.length != 0
                ? ListView.separated(
                    padding: const EdgeInsets.all(8),
                    itemCount: filteredCareTakers.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card1(
                          makeListTile:
                              MakeListTile(item: filteredCareTakers[index]));
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(),
                  )
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ],
    );
  }
}

class MakeListTile extends StatelessWidget {
  final item;
  const MakeListTile({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.94,
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0), side: BorderSide()),
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8.0, top: 8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 2.0),
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.28,
                    maxHeight: MediaQuery.of(context).size.width * 0.28,
                  ),
                  child: Icon(Icons.verified_user_outlined,
                      color: currentTheme.primaryColor,
                      size: size.height * 0.08),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        '${item['name']}',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: const Color(0xff90A2E9)),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 10, 0, 0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: currentTheme.primaryColor),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'email: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    TextSpan(text: item['email']),
                                  ],
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: currentTheme.primaryColor),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'address: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    TextSpan(text: item['address']),
                                  ],
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: currentTheme.primaryColor),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'space: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    TextSpan(
                                        text: item['avaliableSpaceMtSq']
                                                .toString() +
                                            ' sqm'),
                                  ],
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: currentTheme.primaryColor),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'stars: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    TextSpan(text: item['stars'].toString()),
                                  ],
                                ),
                              ),
                            ])),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: Text(
                      'Score',
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: currentTheme.primaryColor),
                    ),
                  ),
                  CircleAvatar(
                    backgroundColor: currentTheme.primaryColor,
                    radius: 30,
                    child: Text(
                      item['score'].toString(),
                      style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// Future data = DefaultAssetBundle.of(context)
//         .loadString("assets/mock/careTakers.json");
//     data.then((value) {
//       final careTakers = json.decode(value);
//       Future data2 = DefaultAssetBundle.of(context)
//           .loadString("assets/mock/petOwners.json");
//       data2.then((value) {
//         petOwner = json.decode(value)[0];
//         var filteredCareTakersTemp = [];
//         for (var caretaker in careTakers) {
//           if (caretaker['pets'].length <= maxNumPetsAccepted &&
//               caretaker['stars'] >= minCalificacionReq &&
//               caretaker['avaliableSpaceMtSq'] >= minSpaceReq) {
//             filteredCareTakers.add(caretaker);
//           }
//           setState(() {
//             filteredCareTakers = filteredCareTakersTemp;
//           });
//         }
//       });
//     });
