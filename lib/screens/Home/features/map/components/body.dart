import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../../../constants/app_constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';

var lat = 4.6694;
var long = -74.0510;

class Body extends StatefulWidget {
  @override
  BodyState createState() => BodyState();
}

class CareTaker {
  final String nombre;
  final LatLng position;
  final int stars;
  final int hour;
  final int day;
  final int week;

  CareTaker(
      this.nombre, this.position, this.stars, this.hour, this.day, this.week);
}

class BodyState extends State<Body> {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  List<CareTaker> _careTakers = [];
  List<CareTaker> _careTakersF = [];
  bool _wait = true;
  double _km = 100.0;
  var subscription;
  var connectionStatus;
  bool _gps = true;
  bool _permission = false;
  LocationPermission permission;
  double zoomVal = 12.0;

  @override
  void initState() {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => connectionStatus = result);
      // checkInternetConnectivity();
    });

    _chekGPS();

    _getCurrentLocation().then((value) {
      setState(() {
        _wait = false;
      });
    });

    super.initState();
  }

  @override
  dispose() {
    super.dispose();
    subscription.cancel();
  }

  Future _chekGPS() async {
    _gps = await Geolocator.isLocationServiceEnabled();
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      print(permission);
      await Geolocator.requestPermission();
      permission = await Geolocator.checkPermission();
      print(permission);
      if (permission == LocationPermission.denied) {
      } else {
        _permission = true;
      }
    } else {
      _permission = true;
    }
  }

  Future _getCurrentLocation() async {
    _firestore
        .collection("careTakers2")
        .orderBy("stars", descending: true)
        .orderBy("day", descending: false)
        .snapshots()
        .listen((event) {
      _careTakers = event.docs
          .map((e) => CareTaker(e['name'], LatLng(e['lat'], e['long']),
              e['stars'], e['hour'], e['day'], e['week']))
          .toList();
      filterMarkers(_km);
      _careTakers = _careTakersF;
    });

    var position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    var lastPosition = await Geolocator.getLastKnownPosition();
    lat = lastPosition.latitude;
    long = lastPosition.longitude;
    lat = position.latitude;
    long = position.longitude;

    _firestore
        .collection("careTakers2")
        .orderBy("stars", descending: true)
        .orderBy("day", descending: false)
        .snapshots()
        .listen((event) {
      _careTakers = event.docs
          .map((e) => CareTaker(e['name'], LatLng(e['lat'], e['long']),
              e['stars'], e['hour'], e['day'], e['week']))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: !_permission
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.error, size: 50, color: Colors.red),
                        Container(
                          height: 50,
                        ),
                        Text('Please enable GPS permissions...'),
                      ],
                    ),
                  )
                : !_gps
                    ? Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(),
                            Container(
                              height: 50,
                            ),
                            Text('Please enable GPS...'),
                          ],
                        ),
                      )
                    : _wait
                        ? Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircularProgressIndicator(),
                                Container(
                                  height: 50,
                                ),
                                Text('Loading...'),
                              ],
                            ),
                          )
                        : Stack(
                            children: <Widget>[
                              _buildGoogleMap(context),
                              _zoomminusfunction(),
                              _zoomplusfunction(),
                              _buildContainer(),
                              _slider()
                            ],
                          )));
  }

  Widget _zoomminusfunction() {
    return Align(
        alignment: Alignment.topLeft,
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            IconButton(
                icon: Icon(FontAwesomeIcons.searchMinus, color: kPrimaryColor),
                onPressed: () {
                  zoomVal--;
                  _minus(zoomVal);
                }),
          ],
        ));
  }

  Widget _zoomplusfunction() {
    return Align(
        alignment: Alignment.topRight,
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            IconButton(
                icon: Icon(FontAwesomeIcons.searchPlus, color: kPrimaryColor),
                onPressed: () {
                  zoomVal++;
                  _plus(zoomVal);
                }),
          ],
        ));
  }

  Widget _slider() {
    return Align(
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            Slider.adaptive(
                value: _km,
                min: 0,
                max: 100,
                divisions: 10,
                label: "Radius of search on KM: " + _km.toString(),
                onChanged: (e) {
                  _careTakersF.clear();
                  filterMarkers(e);
                })
          ],
        ));
  }

  filterMarkers(double e) async {
    var temp;
    var contador = 0;
    while (contador < _careTakers.length) {
      var actual = _careTakers[contador];
      var value = Geolocator.distanceBetween(
          lat, long, actual.position.latitude, actual.position.longitude);
      temp = value / 1000;
      if (temp < e) {
        _careTakersF.add(actual);
      }
      contador++;
    }

    setState(() {
      _km = e;
    });
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: zoomVal)));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: zoomVal)));
  }

  Widget _buildContainer() {
    var _image = "assets/images/noimage.png";

    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        height: 150.0,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _careTakersF.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                _gotoLocation(_careTakersF[index].position.latitude,
                    _careTakersF[index].position.longitude);
              },
              child: Container(
                  child: new FittedBox(
                    child: Material(
                        color: Colors.white,
                        elevation: 14.0,
                        borderRadius: BorderRadius.circular(24.0),
                        shadowColor: Color(0x802196F3),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: 120,
                              height: 150,
                              child: ClipRRect(
                                borderRadius: new BorderRadius.circular(24.0),
                                child: Image(
                                  fit: BoxFit.fill,
                                  image: AssetImage(_image),
                                ),
                              ),
                            ),
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: myDetailsContainer1(
                                    _careTakersF[index].nombre,
                                    _careTakersF[index].stars,
                                    _careTakersF[index].hour,
                                    _careTakersF[index].day,
                                    _careTakersF[index].week),
                              ),
                            ),
                          ],
                        )),
                  ),
                  padding: const EdgeInsets.only(right: 20.0)),
            );
          },
        ),
      ),
    );
  }

  Widget myDetailsContainer1(
      String restaurantName, int stars, int hour, int day, int week) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Container(
              child: Text(
            restaurantName,
            style: TextStyle(
                color: Color(0xff6200ee),
                fontSize: 24.0,
                fontWeight: FontWeight.bold),
          )),
        ),
        SizedBox(height: 5.0),
        Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(child: Row(children: getStars(stars))),
            Container(
                child: Text(
              "(946)",
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18.0,
              ),
            )),
          ],
        )),
        SizedBox(height: 5.0),
        Container(
            child: Text(
          "Cost per",
          style: TextStyle(
              color: Colors.black54,
              fontSize: 18.0,
              fontWeight: FontWeight.bold),
        )),
        SizedBox(height: 5.0),
        Container(
            child: Text(
          "Hour:  $hour ",
          style: TextStyle(
            color: Colors.black54,
            fontSize: 18.0,
          ),
        )),
        SizedBox(height: 2.0),
        Container(
            child: Text(
          "Day:  $day ",
          style: TextStyle(
            color: Colors.black54,
            fontSize: 18.0,
          ),
        )),
        SizedBox(height: 1.0),
        Container(
            child: Text(
          "Week:  $week ",
          style: TextStyle(
            color: Colors.black54,
            fontSize: 18.0,
          ),
        )),
      ],
    );
  }

  Widget _buildGoogleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition:
              CameraPosition(target: LatLng(lat, long), zoom: 12),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
            _mapController = controller;
          },
          markers: generateMarkers(_careTakersF)),
    );
  }

  Set<Marker> generateMarkers(List<CareTaker> l) {
    var result = l
        .map((e) => Marker(
              markerId: MarkerId(e.nombre),
              position: e.position,
              infoWindow: InfoWindow(title: e.nombre),
              icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueViolet,
              ),
            ))
        .toSet();
    result.add(Marker(
      markerId: MarkerId("Your location"),
      position: LatLng(lat, long),
    ));

    return result;
  }

  List<Widget> getStars(int stars) {
    List<Widget> l = List();
    var count = 0;
    for (; stars > 0; stars--) {
      count++;
      l.add(Container(
        child: Icon(
          FontAwesomeIcons.solidStar,
          color: Colors.amber,
          size: 15.0,
        ),
      ));
    }
    while (5 - count != 0) {
      count++;
      l.add(Container(
        child: Icon(
          FontAwesomeIcons.solidStar,
          color: Colors.grey,
          size: 15.0,
        ),
      ));
    }

    return l;
  }

  Future<void> _gotoLocation(double lat, double long) async {
    _mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 15,
      tilt: 50.0,
      bearing: 45.0,
    )));
  }
}
