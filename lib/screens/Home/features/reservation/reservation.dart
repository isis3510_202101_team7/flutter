import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
import 'package:petbnbpn/models/navBarIndexes.dart';
import 'package:provider/provider.dart';

import 'components/starRating.dart';

class Reservation extends StatefulWidget {
  Reservation({Key key}) : super(key: key);

  @override
  _ReservationState createState() => _ReservationState();
}

class _ReservationState extends State<Reservation> {
  final FirebaseFirestore fs = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  dynamic reservation;

  @override
  void initState() {
    super.initState();
    fs.collection("petOwners").doc(auth.currentUser.uid).get().then((value) {
      setState(() {
        reservation = value.data()['reservation'];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return Container(
      padding: EdgeInsets.only(bottom: size.height * 0.02),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey.shade300,
        boxShadow: [
          BoxShadow(color: Colors.transparent, spreadRadius: 3),
        ],
      ),
      width: size.width * 0.8,
      height: size.height * 0.72,
      child: Stack(
        children: [
          Positioned(
              top: -5,
              right: 5,
              child: IconButton(
                icon: Icon(
                  Icons.delete,
                  color: currentTheme.primaryColor,
                  size: size.height * 0.05,
                ),
                onPressed: () {
                  // Navigator.of(context).pop(false);
                  fs
                      .collection("petOwners")
                      .doc(auth.currentUser.uid)
                      .update({"reservation": {}}).then((_) {
                    Provider.of<NavBarIndexes>(context, listen: false)
                        .setLastIndex = 1;
                    Provider.of<NavBarIndexes>(context, listen: false).myIndex =
                        1;
                    print(
                        'Provider.of<NavBarIndexes>(context, listen: false).lastIndex');
                    print(Provider.of<NavBarIndexes>(context, listen: false)
                        .lastIndex);
                    print(
                        'Provider.of<NavBarIndexes>(context, listen: false).myIndex');
                    print(Provider.of<NavBarIndexes>(context, listen: false)
                        .myIndex);
                    //TODO: tiene   que ir a la pag de home/list
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                            'You have successfully deleted your booking!')));
                  }).timeout(Duration(seconds: 2), onTimeout: () {
                    print('timeout');
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text('Check your internet connection.')));
                  });
                },
              )),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: size.height * 0.05, bottom: size.height * 0.02),
                child: Text(
                  "Congrats!",
                  style: TextStyle(
                      fontSize: size.height * 0.07,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: size.height * 0.02),
                child: Icon(
                  Icons.account_circle,
                  size: size.height * 0.2,
                  color: currentTheme.primaryColor,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: size.height * 0.02),
                child: Text("Here is your booking:",
                    style: TextStyle(
                        fontSize: size.height * 0.035,
                        fontWeight: FontWeight.bold)),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: TextStyle(
                            fontSize: size.height * 0.025,
                            // color: currentTheme.primaryColor
                            color: Colors.grey.shade900),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Date: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: reservation != null
                                  ? "${DateTime.fromMillisecondsSinceEpoch(reservation['date'].millisecondsSinceEpoch).year.toString()}-${DateTime.fromMillisecondsSinceEpoch(reservation['date'].millisecondsSinceEpoch).month.toString().padLeft(2, '0')}-${DateTime.fromMillisecondsSinceEpoch(reservation['date'].millisecondsSinceEpoch).day.toString().padLeft(2, '0')}"
                                  : "",
                              style: TextStyle(fontSize: size.height * 0.020)),
                        ],
                      ),
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: TextStyle(
                            fontSize: size.height * 0.025,
                            // color: currentTheme.primaryColor
                            color: Colors.grey.shade900),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Notes: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: reservation != null
                                  ? reservation['notes']
                                  : "",
                              style: TextStyle(fontSize: size.height * 0.020)),
                        ],
                      ),
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: TextStyle(
                            fontSize: size.height * 0.025,
                            // color: currentTheme.primaryColor
                            color: Colors.grey.shade900),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Caretaker: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: reservation != null
                                  ? reservation['careTakerName']
                                  : "",
                              style: TextStyle(fontSize: size.height * 0.020)),
                        ],
                      ),
                    ),
                    StarRating(
                        starCount: 5,
                        rating: reservation != null
                            ? reservation['careTakerStars'].toDouble()
                            : 0,
                        color: Provider.of<ThemeChanger>(context)
                            .currentTheme
                            .primaryColor),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: TextStyle(
                            fontSize: size.height * 0.025,
                            // color: currentTheme.primaryColor
                            color: Colors.grey.shade900),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Address: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: reservation != null
                                  ? reservation['careTakerAddress']
                                  : "",
                              style: TextStyle(fontSize: size.height * 0.020)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // Text("Reserva", style: TextStyle(fontSize: size.height * 0.035)),
            ],
          ),
        ],
      ),
    );
  }
}
