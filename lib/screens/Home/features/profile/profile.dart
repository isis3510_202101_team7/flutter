import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:petbnbpn/screens/Home/features/profile/editProfile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:petbnbpn/screens/Home/features/profile/editPets.dart';
import 'package:petbnbpn/screens/Home/features/profile/imageProfile.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  dynamic data;

  QuerySnapshot petData;

  final FirebaseFirestore fs = FirebaseFirestore.instance;

  Future<dynamic> getData() async {
    final DocumentReference document =
        fs.collection("petOwners").doc(_auth.currentUser.uid);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      setState(() {
        data = snapshot.data();
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
    getPets();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
          actions: [
            IconButton(
                onPressed: () => {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditProfile()))
                          .then((value) => {getData(), getPets()})
                    },
                icon: Icon(CupertinoIcons.pencil))
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: getBody());
  }

  TextStyle _style() {
    return TextStyle(fontWeight: FontWeight.bold);
  }

  Widget getBody() {
    return Center(
        child: Container(
            child: ListView(children: <Widget>[
      Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ImageProfile(),
            SizedBox(height: 15),
            Align(
              alignment: Alignment.center,
              child: Text(
                data == null
                    ? ""
                    : data["name"] == ""
                        ? "User"
                        : data["name"].split(" ")[0],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            SizedBox(height: 15),
            Divider(),
            Text(
              "Full Name",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text(data == null
                ? ""
                : data["name"] == ""
                    ? "Not defined yet"
                    : data["name"]),
            Divider(),
            Text(
              "Email",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text(data == null
                ? ""
                : data["email"] == ""
                    ? "Not defined yet"
                    : data["email"]),
            Divider(),
            Text(
              "Phone Number",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text(data == null
                ? ""
                : data["phone"] == ""
                    ? "Not defined yet"
                    : data["phone"]),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Address",
                      style: _style(),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(data == null
                        ? ""
                        : data["address"] == ""
                            ? "address"
                            : data["address"]),
                  ],
                ),
                Column(
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    // children: <Widget>[Icon(CupertinoIcons.chevron_right)],
                    )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Pet information",
                      style: _style(),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(petData == null ? "" : getPetString()),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                        onPressed: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => EditPets()))
                            },
                        icon: Icon(CupertinoIcons.chevron_right))
                  ],
                )
              ],
            ),
            Divider(),
          ],
        ),
      )
    ])));
  }

  void getPets() async {
    final CollectionReference pets = fs
        .collection("petOwners")
        .doc(_auth.currentUser.uid)
        .collection("pets");

    await pets.get().then<dynamic>((QuerySnapshot snapshot) async {
      setState(() {
        petData = snapshot;
      });
    });
  }

  String getPetString() {
    String pets = "";
    if (petData.size != 0) {
      for (var i = 0; i < petData.size; i++) {
        pets = pets + petData.docChanges[i].doc["name"] + ",";
      }
      pets = pets.substring(0, pets.length - 1);
    }
    return pets;
  }
}
