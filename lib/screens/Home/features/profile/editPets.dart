import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

TextStyle _style() {
  return TextStyle(fontWeight: FontWeight.bold);
}

final FirebaseAuth auth = FirebaseAuth.instance;

final FirebaseFirestore fs = FirebaseFirestore.instance;

class EditPets extends StatefulWidget {
  const EditPets({Key key}) : super(key: key);

  @override
  _EditPetsState createState() => _EditPetsState();
}

QuerySnapshot petData;

class _EditPetsState extends State<EditPets> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit pets'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              onPressed: () => {_displayTextInputDialog(context)},
              icon: Icon(CupertinoIcons.plus))
        ],
      ),
      body: getBody(),
    );
  }

  String valueText;

  String codeDialog;

  TextEditingController _textFieldController = TextEditingController();

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Create new pet'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Name of the pet"),
            ),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('CANCEL'),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.green),
                ),
                onPressed: () {
                  if (valueText != null) {
                    setState(() {
                      codeDialog = valueText;
                      createPet(valueText);
                      Navigator.pop(context);
                    });
                  } else {
                    print('ja gety');
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                            'Your pet must have a name. Please, tell us their name')));
                  }
                },
                child: Text('CONFIRM'),
              ),
            ],
          );
        });
  }

  void createPet(String name) {
    CollectionReference petCollection =
        fs.collection("petOwners").doc(auth.currentUser.uid).collection("pets");

    petCollection.doc(name).set({
      "acceptCats": false,
      "acceptDogs": false,
      "illnesses": [],
      "minSpaceRequired": 0,
      "name": name,
      "size": 0,
      "type": "",
      "vaccines": {
        "multivalent": false,
        "multivalentBoost": false,
        "primary": false,
        "rabies": false
      }
    });

    getPets();
  }

  Widget getBody() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: petData == null
          ? Column()
          : ListView(children: <Widget>[
              for (var i = 0; i < petData.size; i++) getElement(i)
            ]),
    );
  }

  @override
  void initState() {
    super.initState();
    getPets();
  }

  Widget getElement(int i) {
    DocumentSnapshot pet = petData.docChanges[i].doc;
    return Container(
      margin: new EdgeInsets.symmetric(vertical: 5.0),
      padding: new EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color: Colors.deepPurpleAccent[700], // Set border color
            width: 3.0), // Set border width
        borderRadius: BorderRadius.all(
            Radius.circular(10.0)), // Set rounded corner radius
      ),
      child: Column(children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(pet["name"]),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                IconButton(
                    onPressed: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PetDetail(
                                        index: i,
                                      )))
                        },
                    icon: Icon(CupertinoIcons.chevron_right))
              ],
            )
          ],
        ),
      ]),
    );
  }

  void getPets() async {
    final CollectionReference pets =
        fs.collection("petOwners").doc(auth.currentUser.uid).collection("pets");

    await pets.get().then<dynamic>((QuerySnapshot snapshot) async {
      setState(() {
        petData = snapshot;
      });
    });
  }
}

// PET DETAIL EDIT ------------------------------------------------------------

class PetDetail extends StatefulWidget {
  final int index;
  const PetDetail({Key key, this.index}) : super(key: key);
  @override
  PetDetailState createState() => PetDetailState();
}

class PetDetailState extends State<PetDetail> {
  DocumentSnapshot pet;

  void getPets() async {
    final CollectionReference pets =
        fs.collection("petOwners").doc(auth.currentUser.uid).collection("pets");
    await pets.get().then<dynamic>((QuerySnapshot snapshot) async {
      setState(() {
        pet = snapshot.docChanges[widget.index].doc;
        acceptCats = pet["acceptCats"];
        acceptDogs = pet["acceptDogs"];
        nameCtrl.text = pet["name"].toString();
        msrCtrl.text = pet["minSpaceRequired"].toString();
        sizeCtrl.text = pet["size"].toString();
        _selectedText = pet["type"].toString();
      });
    });
  }

  void savePet() {
    fs
        .collection("petOwners")
        .doc(auth.currentUser.uid)
        .collection("pets")
        .doc(pet["name"])
        .update({
      "acceptCats": acceptCats,
      "acceptDogs": acceptDogs,
      "minSpaceRequired": int.parse(msrCtrl.text),
      "name": nameCtrl.text,
      "size": int.parse(sizeCtrl.text),
      "type": _selectedText
    });

    Navigator.pop(context);
  }

  String _selectedText = "";

  bool acceptCats = false;
  bool acceptDogs = false;

  TextEditingController nameCtrl = TextEditingController();
  TextEditingController msrCtrl = TextEditingController();
  TextEditingController sizeCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    getPets();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(pet == null ? "" : pet["name"]),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              onPressed: () => {savePet()},
              icon: Icon(CupertinoIcons.floppy_disk))
        ],
      ),
      body: getPetDetail(),
    );
  }

  Widget getPetDetail() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 15),
            Text(
              "Name",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            TextField(
              controller: nameCtrl,
              decoration: new InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 5),
                errorText: false ? 'Value can\'t be empty' : null,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "Minimum space required",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            TextField(
              controller: msrCtrl,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 5),
                errorText: false ? 'Value can\'t be empty' : null,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "Size",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            TextField(
              controller: sizeCtrl,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 5),
                errorText: false ? 'Value can\'t be empty' : null,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Type",
                      style: _style(),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: _selectedText,
                        items: <String>["", "Dog", "Cat"].map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        onChanged: (String val) {
                          setState(() {
                            _selectedText = val;
                          });
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Accepts Cats",
                      style: _style(),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Checkbox(
                      value: acceptCats,
                      onChanged: (bool value) {
                        setState(() {
                          acceptCats = value;
                        });
                      },
                    ),
                  ],
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Accepts Dogs",
                      style: _style(),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Checkbox(
                      value: acceptDogs,
                      onChanged: (bool value) {
                        setState(() {
                          acceptDogs = value;
                        });
                      },
                    ),
                  ],
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Vaccines",
                      style: _style(),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                        onPressed: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Vaccines(
                                            index: widget.index,
                                          )))
                            },
                        icon: Icon(CupertinoIcons.chevron_right))
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// VACCINES DETAIL ------------------------------------------------------------

class Vaccines extends StatefulWidget {
  final int index;
  const Vaccines({Key key, this.index}) : super(key: key);
  @override
  VaccinesState createState() => VaccinesState();
}

class VaccinesState extends State<Vaccines> {
  DocumentSnapshot pet;

  bool hasPrimary = true;

  bool hasMultivalent = true;

  bool hasMultivalentBoost = true;

  bool hasRabies = true;

  void getPets() async {
    final CollectionReference pets =
        fs.collection("petOwners").doc(auth.currentUser.uid).collection("pets");
    await pets.get().then<dynamic>((QuerySnapshot snapshot) async {
      setState(() {
        pet = snapshot.docChanges[widget.index].doc;
        hasPrimary = pet["vaccines"]["primary"];
        hasMultivalent = pet["vaccines"]["multivalent"];
        hasMultivalentBoost = pet["vaccines"]["multivalentBoost"];
        hasRabies = pet["vaccines"]["rabies"];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getPets();
  }

  void saveVaccines() {
    fs
        .collection("petOwners")
        .doc(auth.currentUser.uid)
        .collection("pets")
        .doc(pet["name"])
        .update({
      "vaccines": {
        "primary": hasPrimary,
        "multivalent": hasMultivalent,
        "multivalentBoost": hasMultivalentBoost,
        "rabies": hasRabies
      }
    });

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Vaccines of ${pet == null ? "" : pet["name"]}"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              onPressed: () => {saveVaccines()},
              icon: Icon(CupertinoIcons.floppy_disk))
        ],
      ),
      body: Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Primary",
                          style: _style(),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Checkbox(
                          value: hasPrimary,
                          onChanged: (bool value) {
                            setState(() {
                              hasPrimary = value;
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
                Divider(
                  color: Colors.black,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Multivalent",
                          style: _style(),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Checkbox(
                          value: hasMultivalent,
                          onChanged: (bool value) {
                            setState(() {
                              hasMultivalent = value;
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
                Divider(
                  color: Colors.black,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Multivalent Boost",
                          style: _style(),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Checkbox(
                          value: hasMultivalentBoost,
                          onChanged: (bool value) {
                            setState(() {
                              hasMultivalentBoost = value;
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
                Divider(
                  color: Colors.black,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Rabies",
                          style: _style(),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Checkbox(
                          value: hasRabies,
                          onChanged: (bool value) {
                            setState(() {
                              hasRabies = value;
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
                Divider(
                  color: Colors.black,
                ),
              ])),
    );
  }
}
