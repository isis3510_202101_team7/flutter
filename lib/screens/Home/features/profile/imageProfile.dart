import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cached_network_image/cached_network_image.dart';

class ImageProfile extends StatefulWidget {
  const ImageProfile({Key key}) : super(key: key);

  @override
  _ImageProfileState createState() => _ImageProfileState();
}

class _ImageProfileState extends State<ImageProfile> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  CollectionReference imgRef;
  firebase_storage.Reference ref;

  PickedFile imageFile;

  final ImagePicker picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: <Widget>[
        SizedBox(height: 15),
        Stack(
          children: <Widget>[
            CircleAvatar(
                radius: 80.0,
                backgroundImage: imageFile != null
                    ? FileImage(File(imageFile.path))
                    : _auth.currentUser.photoURL == null
                        ? AssetImage("assets/images/noimage.png")
                        : CachedNetworkImageProvider(
                            _auth.currentUser.photoURL)),
            Positioned(
              bottom: 20.0,
              right: 20.0,
              child: InkWell(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: ((builder) => bottomSheet()),
                  );
                },
                child: Icon(
                  Icons.camera_alt,
                  color: Colors.orange,
                  size: 28.0,
                ),
              ),
            ),
          ],
        ),
      ],
    ));
  }

  Widget bottomSheet() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          Text(
            "Choose profile photo",
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextButton.icon(
                icon: Icon(Icons.camera),
                onPressed: () {
                  takePhoto(ImageSource.camera);
                },
                label: Text("Camera"),
              ),
              TextButton.icon(
                icon: Icon(Icons.image),
                onPressed: () {
                  takePhoto(ImageSource.gallery);
                },
                label: Text("Gallery"),
              )
            ],
          )
        ],
      ),
    );
  }

  void takePhoto(ImageSource source) async {
    final pickedFile = await picker.getImage(
      source: source,
    );

    setState(() {
      imageFile = pickedFile;
    });

    uploadFile();
  }

  Future uploadFile() async {
    ref = firebase_storage.FirebaseStorage.instance
        .ref()
        .child('profiles/${_auth.currentUser.uid}.png');

    await ref.putFile(File(imageFile.path)).whenComplete(() async {
      await ref.getDownloadURL().then((value) => {
            imgRef.add({'url': value}),
            updateProfilePic(value)
          });
    });
  }

  void updateProfilePic(String url) async {
    var user = FirebaseAuth.instance.currentUser;
    user
        .updatePhotoURL(url)
        .then((value) => print("Succesfully updated"))
        .catchError((e) {
      print("Error updating");
    });
  }

  @override
  void initState() {
    super.initState();
    imgRef = FirebaseFirestore.instance.collection('imageURLs');
  }
}
