import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  bool validatePhone = false;

  bool validateName = false;

  bool validateAddress = false;

  final FirebaseAuth auth = FirebaseAuth.instance;

  final FirebaseFirestore fs = FirebaseFirestore.instance;
  dynamic data;

  TextEditingController nameController = TextEditingController();

  TextEditingController phoneController = TextEditingController();

  TextEditingController addressController = TextEditingController();

  Future<dynamic> getData() async {
    final DocumentReference document =
        fs.collection("petOwners").doc(auth.currentUser.uid);
    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      setState(() {
        data = snapshot.data();
      });
    });

    nameController.text = data['name'];
    phoneController.text = data['phone'];
    addressController.text = data['address'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit profile'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              onPressed: () => saveData(context),
              icon: Icon(CupertinoIcons.floppy_disk))
        ],
      ),
      body: getBody(),
    );
  }

  void saveData(BuildContext context) {
    setState(() {
      phoneController.text.isEmpty
          ? validatePhone = true
          : validatePhone = false;
      nameController.text.isEmpty ? validateName = true : validateName = false;
      addressController.text.isEmpty
          ? validateAddress = true
          : validateAddress = false;
    });

    if (nameController.text != "" &&
        phoneController.text != "" &&
        addressController.text != "") {
      fs.collection("petOwners").doc(auth.currentUser.uid).update({
        "name": nameController.text,
        "phone": phoneController.text,
        "address": addressController.text
      });

      Navigator.pop(context);
    } else {}
  }

  Widget getBody() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            controller: nameController,
            decoration: new InputDecoration(
              hintText: "Enter your full name",
              labelText: "Full name",
              border: OutlineInputBorder(),
              errorText: validateName ? 'Value can\'t be empty' : null,
            ),
          ),
          SizedBox(height: 15),
          TextField(
            controller: phoneController,
            decoration: new InputDecoration(
              hintText: "Enter your phone number",
              labelText: "Phone number",
              border: OutlineInputBorder(),
              errorText: validatePhone ? 'Value can\'t be empty' : null,
            ),
          ),
          SizedBox(height: 15),
          TextField(
            controller: addressController,
            decoration: new InputDecoration(
              hintText: "Enter your Address",
              labelText: "Address",
              border: OutlineInputBorder(),
              errorText: validateAddress ? 'Value can\'t be empty' : null,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getData();
  }
}
