import 'package:flutter/material.dart';
import 'package:petbnbpn/constants/app_constants.dart';

class ConnectivityStrategyWdg extends StatelessWidget {
  const ConnectivityStrategyWdg({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
              "We are sorry to tell you  there is no connectivity to complete the request, please make sure you have internet access.",
              style: TextStyle(fontWeight: FontWeight.bold)),
          Container(
            height: 50,
          ),
          Icon(
            Icons.wifi_off,
            color: kPrimaryColor,
          )
        ],
      ),
      padding: EdgeInsets.all(50),
    );
  }
}
