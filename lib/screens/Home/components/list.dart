import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
// import 'package:petbnbpn/models/user.dart';
import 'package:provider/provider.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'formDialogue.dart';

class MainList extends StatefulWidget {
  const MainList({Key key}) : super(key: key);

  @override
  _MainListState createState() => _MainListState();
}

class _MainListState extends State<MainList> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseFirestore fs = FirebaseFirestore.instance;
  bool showAddIcon = true;

  void showAddIconToggle(bool param) {
    setState(() {
      showAddIcon = param;
    });
  }

  QuerySnapshot careTakersData;

  int cantidad;

  List<DocumentChange> list;

  @override
  Widget build(BuildContext context) {
    var _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          cantidad = cantidad + 10;
        });
      }
    });

    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    if (careTakersData != null) {
      list = careTakersData.docChanges;

      return ListView(
        controller: _scrollController,
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: cantidad,
              itemBuilder: (context, index) {
                return getCard(currentTheme, list[index].doc);
              })
        ],
      );
    } else {
      return Container();
    }
  }

  Widget getCard(ThemeData currentTheme, DocumentSnapshot careTaker) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
            side: BorderSide(
              color: currentTheme.primaryColor,
              width: 3,
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ListTile(
                trailing: showAddIcon
                    ? IconButton(
                        icon: Icon(Icons.add,
                            color: currentTheme.primaryColor, size: 35),
                        onPressed: () {
                          showDialog(
                              barrierDismissible: true,
                              context: context,
                              builder: (BuildContext context) =>
                                  FormDialogueReservation(
                                      careTaker: careTaker,
                                      toggleAddIcon: showAddIconToggle));
                        })
                    : Container(
                        height: 0,
                        width: 0,
                      ),
                leading: Icon(Icons.person, size: 45),
                title: Text(careTaker["name"]),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(careTaker["email"]),
                    RatingBar.builder(
                      initialRating: careTaker["stars"].toDouble(),
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 15,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void loadCareTakers() async {
    await fs
        .collection("careTakers")
        .get()
        .then<dynamic>((QuerySnapshot snapshot) async {
      setState(() {
        careTakersData = snapshot;
      });
    });
  }

  @override
  void initState() {
    super.initState();

    cantidad = 15;

    loadCareTakers();

    fs.collection("petOwners").doc(auth.currentUser.uid).get().then((value) {
      if (value.data() != null &&
          value.data()['reservation'] != null &&
          value.data()['reservation']['state'] == true) {
        setState(() {
          showAddIcon = false;
        });
      }
    });
  }
}
