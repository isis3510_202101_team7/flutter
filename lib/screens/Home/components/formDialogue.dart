import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:petbnbpn/components/rounded_button.dart';
import 'package:petbnbpn/components/rounded_input_field.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
import 'package:provider/provider.dart';

class FormDialogueReservation extends StatefulWidget {
  var careTaker;
  void Function(bool param) toggleAddIcon;

  FormDialogueReservation(
      {Key key,
      @required this.careTaker,
      @required Function(bool param) this.toggleAddIcon})
      : super(key: key);

  @override
  _FormDialogueReservationState createState() =>
      _FormDialogueReservationState();
}

class _FormDialogueReservationState extends State<FormDialogueReservation> {
  final _formKey = GlobalKey<FormState>();
  final FirebaseAuth auth = FirebaseAuth.instance;
  DateTime chosenDate;
  String notes;
  String error;

  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    // var appUser = Provider.of<AppUser>(context);
    final firestoreInstance = FirebaseFirestore.instance;
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: AlertDialog(
        content: Stack(
          children: <Widget>[
            Positioned(
                top: -10,
                right: -15,
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                )),
            Container(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 30),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          style: TextStyle(
                              fontSize: size.width / 20,
                              // color: currentTheme.primaryColor
                              color: Colors.grey.shade600),
                          children: <TextSpan>[
                            TextSpan(
                              text:
                                  'Are you sure you want to make a reservation with ',
                            ),
                            TextSpan(
                                text: '${widget.careTaker["name"]}?',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ),
                    RoundedInputField(
                      icon: Icons.note,
                      hintText: "Notes to caretaker",
                      onChanged: (value) {
                        setState(() {
                          notes = value;
                        });
                      },
                    ),
                    RoundedButton(
                      text: "Pick your date",
                      press: () {
                        showDatePicker(
                                context: context,
                                initialDate: new DateTime.now(),
                                firstDate: new DateTime.now(),
                                lastDate: new DateTime.utc(
                                    2022, DateTime.november, 9))
                            .then((value) {
                          error = "";
                          setState(() {
                            chosenDate = value;
                          });
                        });
                      },
                      textColor: chosenDate == null
                          ? currentTheme.primaryColor
                          : Colors.white, //cambiar cuando ya esté la fecha
                      color: chosenDate == null
                          ? currentTheme.primaryColorLight
                          : currentTheme
                              .primaryColor, //cambiar cuando ya esté la fecha
                    ),
                    RoundedButton(
                      text: "Book it already!",
                      press: () {
                        setState(() {
                          error = "";
                        });
                        if (chosenDate == null) {
                          setState(() {
                            error = "Please choose your date";
                          });
                        } else {
                          firestoreInstance
                              .collection("petOwners")
                              .doc(auth.currentUser.uid)
                              .update({
                            "reservation": {
                              'state': true,
                              'notes': notes,
                              'date': chosenDate,
                              'careTakerName': widget.careTaker["name"],
                              'careTakerStars': widget.careTaker["stars"],
                              'careTakerAddress': widget.careTaker["address"],
                            }
                          }).then((_) {
                            // print("success!");
                            widget.toggleAddIcon(false);
                            Navigator.of(context).pop(false);
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content:
                                    Text('You have successfully booked!')));
                          }).timeout(Duration(seconds: 2), onTimeout: () {
                            print('timeout');
                            setState(() {
                              error = 'Please check your internet connection';
                            });
                          });
                          // print(appUser.user.uid);
                        }
                      },
                    ),
                    Text(
                      error == null ? "" : error,
                      style: TextStyle(
                          color: Colors.red, fontSize: size.width / 30),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
