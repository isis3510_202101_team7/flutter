import 'package:flutter/material.dart';
import 'package:petbnbpn/screens/Home/components/list.dart';

// THIS MUST BE STATEFULL IF THE USER IS NEEDED
class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainList();
  }
}
