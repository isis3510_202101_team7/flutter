import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
import 'package:petbnbpn/models/navBarIndexes.dart';
import 'package:petbnbpn/screens/Home/features/profile/profile.dart';
import 'package:petbnbpn/screens/Home/features/recomendations/recomendations.dart';
import 'package:petbnbpn/services/auth.dart';
import 'package:provider/provider.dart';
import 'Components/body.dart';
import 'components/connectivityStrategy.dart';
import 'features/map/map_screen.dart';
import 'package:connectivity/connectivity.dart';
import 'features/reservation/reservation.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseFirestore fs = FirebaseFirestore.instance;
  var connectionStatus;
  bool hayConexion = false;
  int myIndex = 1;
  int lastIndex = 0;
  final tabs = [
    Center(child: ConnectivityStrategyWdg()),
    Center(child: Body()),
    Center(child: MapScreen()),
    Center(child: Recommendation()),
    Center(child: Reservation()),
  ];

  @override
  void initState() {
    Connectivity().checkConnectivity().then((value) {
      setState(() {
        connectionStatus = value;
        print(connectionStatus);
        if (connectionStatus == ConnectivityResult.none) {
          hayConexion = false;
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 0;
        } else {
          hayConexion = true;
        }
      });
    });
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() {
        connectionStatus = result;
        if (connectionStatus == ConnectivityResult.none) {
          hayConexion = false;
          Provider.of<NavBarIndexes>(context).myIndex = 0;
        } else {
          hayConexion = true;
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final AuthService _auth = AuthService();
    Size size = MediaQuery.of(context).size;
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: AppBar(
            title: Text('PetBnB'),
            // centerTitle: true,
            titleSpacing: size.width / 8,
            automaticallyImplyLeading: false,
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Profile()),
                    );
                  },
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  )),
              TextButton(
                onPressed: () async {
                  await _auth.logOut();
                },
                child: Text(
                  "Logout",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          body: tabs[Provider.of<NavBarIndexes>(context).myIndex],
          bottomNavigationBar: BottomNavigationBar(
              unselectedItemColor: Colors.grey.shade600,
              showSelectedLabels: true,
              showUnselectedLabels: false,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.map),
                  label: 'Map',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.recommend),
                  label: 'Recommend',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.book),
                  label: 'Reserve',
                )
              ],
              selectedLabelStyle: TextStyle(fontSize: size.width / 32),
              selectedItemColor: currentTheme.primaryColor,
              // backgroundColor: currentTheme.primaryColor,

              currentIndex: Provider.of<NavBarIndexes>(context).lastIndex,
              onTap: _onItemTapped,
              type: BottomNavigationBarType.fixed),
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    if (index == 0) {
      FirebaseAnalytics().logEvent(name: "OpenedFunctionalityHome");
      setState(() {
        if (!hayConexion) {
          print("esto pasa");
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 0;
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 0;
        } else {
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 1;
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 0;
        }
      });
    }
    if (index == 1) {
      FirebaseAnalytics().logEvent(name: "OpenedFunctionalityMap");
      setState(() {
        if (!hayConexion) {
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 1;
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 0;
        } else {
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 2;
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 1;
        }
      });
    }
    if (index == 2) {
      FirebaseAnalytics().logEvent(name: "OpenedFunctionalityRecommendations");
      setState(() {
        if (!hayConexion) {
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 2;
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 0;
        } else {
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 3;
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 2;
        }
      });
    }
    if (index == 3) {
      FirebaseAnalytics().logEvent(name: "OpenedFunctionalityResevations");
      if (!hayConexion) {
        setState(() {
          Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 3;
          Provider.of<NavBarIndexes>(context, listen: false).myIndex = 0;
        });
      } else {
        // if there is connectivity
        fs
            .collection("petOwners")
            .doc(auth.currentUser.uid)
            .get()
            .then((value) {
          if (value.data() != null &&
              value.data()['reservation'] != null &&
              value.data()['reservation']['state'] == true) {
            //If the user has a booking
            setState(() {
              Provider.of<NavBarIndexes>(context, listen: false).myIndex = 4;
              Provider.of<NavBarIndexes>(context, listen: false).lastIndex = 3;
            });
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('You have no reservations yet')));
          }
        });
      }
    }
  }
}
