import 'package:flutter/material.dart';
import 'package:petbnbpn/constants/app_constants.dart';

class ThemeChanger with ChangeNotifier {
  bool _darkTheme = false;

  ThemeData _currentTheme;

  ThemeChanger(int theme) {
    switch (theme) {
      case 1:
        _darkTheme = false;
        // _currentTheme = ThemeData.dark().copyWith(accentColor: Colors.pink);
        _currentTheme = ThemeData.light().copyWith(
          primaryColor: kPrimaryColor,
          primaryColorLight: kPrimaryLightColor,
          scaffoldBackgroundColor: Colors.white,
          accentColor: kAccentColor,
          // buttonTheme: ButtonThemeData(splashColor: Colors.red),
        );
        break;
      case 2:
        _darkTheme = true;
        _currentTheme = ThemeData.dark();
        break;
      default:
        _darkTheme = false;
        _currentTheme = ThemeData.light();
    }
  }

  bool get darkTheme => this._darkTheme;
  ThemeData get currentTheme => this._currentTheme;

  set darkTheme(bool value) {
    _darkTheme = value;
    if (value) {
      // _currentTheme = ThemeData.dark().copyWith(accentColor: Colors.pink);
      _currentTheme = ThemeData.dark();
    } else {
      _currentTheme = ThemeData.light().copyWith(accentColor: Colors.pink);
    }
    notifyListeners();
  }
}
