import 'package:flutter/material.dart';
import 'package:petbnbpn/config/themes/theme_config.dart';
import 'package:provider/provider.dart';

class Card1 extends StatelessWidget {
  final makeListTile;
  const Card1({@required this.makeListTile, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(
            color:
                Provider.of<ThemeChanger>(context).currentTheme.primaryColor),
        child: makeListTile,
      ),
    );
    
  }
}
