import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/services.dart';
import './config/themes/theme_config.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'models/navBarIndexes.dart';
import 'models/user.dart';
import 'screens/wrapper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runZonedGuarded(() {
      runApp(MultiProvider(providers: [
        ChangeNotifierProvider<ThemeChanger>.value(value: ThemeChanger(1)),
        ChangeNotifierProvider<NavBarIndexes>.value(
            value: NavBarIndexes(lastIndex: 0, myIndex: 1)),
        ChangeNotifierProvider<AppUser>.value(
            value: new AppUser(user: FirebaseAuth.instance.currentUser)),
        // ChangeNotifierProvider<AppUser>.value(value: ThemeChanger(1)),
        //ChangeNotifierProvider<LayoutModel>.value(value: LayoutModel()),
      ], child: MyApp()));
    }, (error, stackTrace) {
      FirebaseCrashlytics.instance.recordError(error, stackTrace);
    });
  });
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      debugShowCheckedModeBanner: false,
      title: 'PetBnb',
      theme: Provider.of<ThemeChanger>(context).currentTheme,
      home: Wrapper(),
      // home: Home(),
    );
  }
}
