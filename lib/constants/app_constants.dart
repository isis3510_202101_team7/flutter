import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF6F35A5);
// const kPrimaryLightColor = Color(0xFFF1E6FF);
const kPrimaryLightColor = Color(0xFFDBD0F2);
const kAccentColor = Color(0xFF1C1240);
