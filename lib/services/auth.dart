import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  UserCredential credential;

  // create user obj based on firebase user
  // AppUser _userFromFirebaseUser(User user) {
  //   return user != null ? AppUser(user: user) : null;
  // }

  // auth change user stream
  // Stream<User> get user {
  //   return _auth.onAuthStateChanged
  //.map((FirebaseUser user) => _userFromFirebaseUser(user));
  // .map(_userFromFirebaseUser);

  // FirebaseAuth.instance.authStateChanges().listen((User user) {
  //   if (user == null) {
  //     return Wrapper();
  //   } else {
  //     return Home();
  //   }
  // });
  // }

  // sign in anon
  Future signInAnon() async {
    try {
      UserCredential result = await _auth.signInAnonymously();
      User user = result.user;
      print("successfuly signed in");
      // return _userFromFirebaseUser(user);
      return user;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future logInEmailAndPassword({email, pass}) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: pass);
      FirebaseAnalytics().logSignUp(signUpMethod: "logInEmailAndPassword");
      return userCredential;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        // print('No user found for that email.');
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        // print('Wrong password provided for that user.');
        return 'Wrong password or username.';
      } else {
        // print(e.message);
        return e.message;
      }
    } catch (e, s) {
      FirebaseCrashlytics.instance
          .recordError(e, s, reason: 'Unexpected error when loging in.');
      return e.message;
    }
  }

  Future logOut() async {
    try {
      await _auth.signOut();
      FirebaseAnalytics().logEvent(name: "LogOut");
      print("successfuly signed out");
    } catch (e, s) {
      // print(e.toString());
      FirebaseCrashlytics.instance
          .recordError(e, s, reason: 'Unexpected error when Loging Out.');
    }
  }

  Future registration({email, pass}) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: pass);
      FirebaseAnalytics()
          .logSignUp(signUpMethod: "createUserWithEmailAndPassword");
      User user = userCredential.user;
      await FirebaseFirestore.instance
          .collection("petOwners")
          .doc(user.uid)
          .set({
        "address": "",
        "email": email,
        "name": "",
        "phone": "",
        "userId": user.uid
      });

      return userCredential;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        // print('The password provided is too weak.');
        return ('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        // print('The account already exists for that email.');
        return ('The account already exists for that email.');
      } else {
        // print(super)
        return e.message;
      }
    } catch (e, s) {
      // print(e.toString());
      FirebaseCrashlytics.instance
          .recordError(e, s, reason: 'Unexpected error when registering.');
      return e.message;
    }
  }
}
