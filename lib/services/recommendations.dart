import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:petbnbpn/services/realDb.dart';

class RecommendationsService {
  final dBService = DBService();

  Future<List<dynamic>> filteredCareTakers({@required petOwner}) async {
    List<dynamic> filteredCareTakers = [];
    var careTakers =
        await dBService.getFromFirebaseRTDB(path: "/careTakersData");

    // print("petOwner $petOwner");
    // print("careTakers ${careTakers.length}");
    var minCalificacionReqq = petOwner["minCalificacionReqq"];
    var maxNumberPetsAccepted = petOwner["maxNumberPetsAccepted"];
    var minSpaceRequired = petOwner["pets"][0]["minSpaceRequired"];
    var acceptDogs = petOwner["pets"][0]["acceptDogs"];
    var acceptCats = petOwner["pets"][0]["acceptCats"];

    //10 puntos: se resta si la caracteristica es negativa únicamente (no se suma si es positivo)
    // rating higher than (-2)
    // max of pets in the house (-2)
    // min space avaliable in the house (-2) *
    // has dogs (-2) *
    // has cats (-2) *
    var cTScore = 10;
    var cTTieneGato = false;
    var cTTienePerro = false;
    if (careTakers != null && careTakers.length != 0) {
      for (var caretaker in careTakers) {
        // print(caretaker);
        cTTieneGato =
            busqueda(arreglo: caretaker["pets"], attrib: "type", valor: "cat");
        cTTienePerro =
            busqueda(arreglo: caretaker["pets"], attrib: "type", valor: "dog");
        if (!acceptCats && cTTieneGato) {
          cTScore = cTScore - 2;
        }
        if (!acceptDogs && cTTienePerro) {
          cTScore = cTScore - 2;
        }
        if (minSpaceRequired > caretaker['avaliableSpaceMtSq']) {
          cTScore = cTScore - 2;
        }
        if (caretaker["pets"] != null &&
            maxNumberPetsAccepted < caretaker["pets"].length) {
          cTScore = cTScore - 2;
        }
        if (minCalificacionReqq > caretaker["stars"]) {
          cTScore = cTScore - 2;
        }

        //meta score al caretaker
        filteredCareTakers.add({
          "name": caretaker["name"],
          "email": caretaker["email"],
          "address": caretaker["address"],
          "stars": caretaker["stars"],
          "avaliableSpaceMtSq": caretaker["avaliableSpaceMtSq"],
          "score": cTScore
        });
        // caretaker.add({"score": cTScore});
        // print(cTScore);
        cTScore = 10;
      }
      filteredCareTakers.sort((a, b) => a["score"] < b["score"] ? 1 : -1);

      // Delete duplicates (only left 3 duplicates per score)
      var toRemove = [];
      var localScore = 10;
      var cuentaLocal = 0;
      filteredCareTakers.forEach((item) {
        if (localScore > item['score']) {
          localScore = item['score'];
          cuentaLocal = 0;
        } else if (localScore == item['score']) {
          if (cuentaLocal == 2) {
            toRemove.add(item);
          } else {
            cuentaLocal++;
          }
        }
      });
      filteredCareTakers.removeWhere((element) => toRemove.contains(element));
    }
    return filteredCareTakers;
  }

  bool busqueda({dynamic arreglo, String attrib, dynamic valor}) {
    // print(arreglo);
    // print(attrib);
    // print(valor);
    if (arreglo == null || arreglo.length == 0) {
      return false;
    } else {
      for (var i in arreglo) {
        if (i[attrib] == valor) {
          return true;
        }
      }
      return false;
    }
  }
}
