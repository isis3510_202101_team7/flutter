import 'package:firebase_database/firebase_database.dart';

class DBService {
  final databaseRef = FirebaseDatabase.instance.reference();

  void addData(String data) {
    databaseRef.push().set({'name': data, 'comment': 'A good season'});
  }

  void printFirebase({String path = "/"}) {
    databaseRef.child(path).once().then((DataSnapshot snapshot) {
      print('Data : ${snapshot.value}');
    });
  }

  dynamic getFromFirebaseRTDB({String path = "/"}) async {
    // databaseRef.child(path).once().then((DataSnapshot snapshot) {
    //   print("path $path");
    //   print("snapshot.value ${snapshot.value}");
    //   return snapshot.value;
    // });
    DataSnapshot x = await databaseRef.child(path).once();
    return x.value;
  }
}
