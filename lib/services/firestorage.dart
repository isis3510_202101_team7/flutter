import 'package:cloud_firestore/cloud_firestore.dart';

class FirestorageService {
  final firestoreRef = FirebaseFirestore.instance;

  void addData(String data) {
    // databaseRef.push().set({'name': data, 'comment': 'A good season'});
  }

  void printFirebase({String path = "/"}) {
    // databaseRef.child(path).once().then((DataSnapshot snapshot) {
    //   print('Data : ${snapshot.value}');
    // });
  }

  // dynamic getCollection({String path = ""}) async {
  Future<QuerySnapshot> getCollection({String path = ""}) async {
    QuerySnapshot snapshot =
        await FirebaseFirestore.instance.collection(path).get();
    // return snapshot.docs.map(doc => doc.data());
    // Query collectionRef = FirebaseFirestore.instance.collectionGroup(path);
    // print('ENTRA A GETCOLLECTION');
    // print(collectionRef.get());
    return snapshot;
  }
}
